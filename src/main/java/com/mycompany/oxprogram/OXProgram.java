/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.oxprogram;

import java.util.Scanner;

/**
 *
 * @author roman
 */
public class OXProgram {

    static char table[][] = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    static char currentPlayer = 'O';
    static int row, col;
    static Scanner kb = new Scanner(System.in);
    static boolean finish = false;
    static int count = 0;

    public static void main(String[] args) {
        showWelcome();
        while (true) {
            showTable(table);
            showTurn();
            inputRowCol();
            process();
            if (finish) {
                break;
            }
        }
    }

    public static void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    private static void showTable(char[][] table) {
        for (int r = 0; r < table.length; r++) {
            for (int c = 0; c < table[r].length; c++) {
                System.out.print(table[r][c]);
            }
            System.out.println("");
        }
    }

    private static void showTurn() {
        System.out.println("Turn" + currentPlayer);
    }

    public static void inputRowCol() {
        System.out.println("Please input row,col: ");
        row = kb.nextInt();
        col = kb.nextInt();
    }

    private static void process() {
        setTable();
        if (checkWin()) {
            finish = true;
            ShowWin();
            return;
        }
        count++;
        if (checkDraw()) {
            finish = true;
            ShowDraw();
        }
        switchPlayer();

    }

    private static void switchPlayer() {
        if (currentPlayer == 'O') {
            currentPlayer = 'X';
        } else {
            currentPlayer = 'O';
        }

    }

    public static void setTable() {
        table[row - 1][col - 1] = currentPlayer;

    }

    public static boolean checkWin() {
        if (checkVertical()) {
            return true;
        } else if (checkHorizontal()) {
            return true;
        } else if (checkDia()) {
            return true;
        }

        return false;

    }

    public static boolean checkVertical() {
        for (int r = 0; r < table.length; r++) {
            if (table[r][col - 1] != currentPlayer) {
                return false;
            }
        }
        return true;

    }

    public static boolean checkHorizontal() {
        for (int c = 0; c < table.length; c++) {

            if (table[row - 1][c] != currentPlayer) {
                return false;
            }
        }
        return true;

    }

    public static boolean checkDia() {
        if (check_X()) {
            return true;

        } else if (check_X2()) {
            return true;
        }
        return false;
    }

    public static boolean check_X() {
        for (int i = 0; i < table.length; i++) {
            if (table[i][i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    public static boolean check_X2() {
        for (int i = 0; i < table.length; i++) {
            if (table[i][2 - i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    public static void ShowWin() {
        showTable(table);
        System.out.println(">> " + currentPlayer + " <<");
    }

    public static boolean checkDraw() {
        if (count == 9) {
            return true;
        }
        return false;
    }

    public static void ShowDraw() {
        showTable(table);
        System.out.println(">> Draw <<");
    }

}
