/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.oxprogram;

import com.roman.oxprogram.Example;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author roman
 */
public class TDDTest {

    public TDDTest() {
    }

    public void testAdd_1_2is3() {
        assertEquals(3, Example.add(1, 2));
    }

    public void testAdd_3_4is7() {
        assertEquals(7, Example.add(3, 4));
    }

    public void testAdd_20_22is42() {
        assertEquals(42, Example.add(20, 22));
    }

    public void testchup_p1_p_p2_p_is_draw() {
        assertEquals("draw", Example.chup('p', 'p'));
    }

    public void testchup_p1_h_p2_h_is_draw() {
        assertEquals("draw", Example.chup('h', 'h'));
    }

    public void testchup_p1_s_p2_p_is_p1() {
        assertEquals("p1", Example.chup('s', 'p'));
    }

    public void testchup_p1_h_p2_s_is_p1() {
        assertEquals("p1", Example.chup('h', 's'));
    }

    public void testchup_p1_p_p2_h_is_p1() {
        assertEquals("p1", Example.chup('p', 'h'));
    }
         
   
     public void testchup_p1_h_p2_p_is_p1(){
       assertEquals("p2", Example.chup('h','p'));
    }

     public void testchup_p1_p_p2_s_is_p1(){
       assertEquals("p2", Example.chup('p','s'));
    }
     

     public void testchup_p1_s_p2_h_is_p1(){
       assertEquals("p2", Example.chup('s','h'));
    }


}
