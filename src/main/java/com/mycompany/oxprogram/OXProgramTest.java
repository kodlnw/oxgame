/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.oxprogram;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author roman
 */
public class OXProgramTest {

    public OXProgramTest() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    @Test
    public void testCheckVerticalPlayerOCal1() {
        char currentPlayer = 'O';
        char table[][] = {{'O', '-', '-'},
        {'O', '-', '-'},
        {'O', '-', '-'}};
        int col = 1;
        assertEquals(true, OXProgram.checkVertical(table, currentPlayer, col));
    }

    @Test
    public void testCheckVerticalPlayerOCal2() {
        char currentPlayer = 'O';
        char table[][] = {{'-', 'O', '-'},
        {'-', 'O', '-'},
        {'-', 'O', '-'}};
        int col = 2;
        assertEquals(true, OXProgram.checkVertical(table, currentPlayer, col));
    }

    @Test
    public void testCheckVerticalPlayerOCal3() {
        char currentPlayer = 'O';
        char table[][] = {{'-', '-', 'O'},
        {'-', '-', 'O'},
        {'-', '-', 'O'}};
        int col = 3;
        assertEquals(true, OXProgram.checkVertical(table, currentPlayer, col));
    }

    @Test
    public void testCheckVerticalPlayerOCal1False() {
        char currentPlayer = 'O';
        char table[][] = {{'O', '-', '-'},
        {'O', '-', '-'},
        {'-', 'O', '-'}};
        int col = 1;
        assertEquals(false, OXProgram.checkVertical(table, currentPlayer, col));
    }

    @Test
    public void testCheckVerticalPlayerOCal2False() {
        char currentPlayer = 'O';
        char table[][] = {{'-', 'O', '-'},
        {'-', 'O', '-'},
        {'-', '-', 'O'}};
        int col = 2;
        assertEquals(false, OXProgram.checkVertical(table, currentPlayer, col));
    }

    @Test
    public void testCheckVerticalPlayerOCal3False() {
        char currentPlayer = 'O';
        char table[][] = {{'-', '-', 'O'},
        {'-', '-', 'O'},
        {'-', 'O', '-'}};
        int col = 3;
        assertEquals(false, OXProgram.checkVertical(table, currentPlayer, col));
    }

    @Test
    public void testCheckHorizontalPlayerORow1False() {
        char currentPlayer = 'O';
        char table[][] = {{'O', 'O', '-'},
        {'-', '-', 'O'},
        {'-', '-', '-'}};
        int row = 1;
        assertEquals(false, OXProgram.checkHorizontal(table, currentPlayer, row));
    }

    @Test
    public void testCheckHorizontalPlayerORow2False() {
        char currentPlayer = 'O';
        char table[][] = {{'-', '-', '-'},
        {'O', 'O', '-'},
        {'-', '-', 'O'}};
        int row = 2;
        assertEquals(false, OXProgram.checkHorizontal(table, currentPlayer, row));
    }

    @Test
    public void testCheckHorizontalPlayerORow3False() {
        char currentPlayer = 'O';
        char table[][] = {{'-', '-', '-'},
        {'-', '-', 'O'},
        {'O', 'O', '-'}};
        int row = 3;
        assertEquals(false, OXProgram.checkHorizontal(table, currentPlayer, row));
    }
        
    @Test
        public void testCheckXRightPlayerOFalse() {
        char currentPlayer = 'O';
        char table[][] = {{'-', '-', 'O'},
                                                             {'-', 'O', '-'},
                                                             {'O', '-', '-'}};
        assertEquals(false, OXProgram.checkX1(table, currentPlayer));
    }
        
       @Test
        public void testCheckXLeftPlayerOFalse() {
        char currentPlayer = 'O';
        char table[][] = {{'O', '-', '-'},
                                                             {'-', 'O', '-'},
                                                             {'-', '-', 'O'}};
        assertEquals(false, OXProgram.checkX2(table, currentPlayer));
    }
            
     //testCheckDrawPlayerO
     @Test
        public void testCheckDraw() {
        int count = 9;
        assertEquals(true, OXProgram.checkDraw(count));
    }
                
     
    //testCheckXPlayerO
    @Test
        public void testCheckXRPlayerO() {
        char currentPlayer = 'O';
        char table[][] = {{'O', '-', '-'},
                                                             {'-', 'O', '-'},
                                                             {'-', '-', 'O'}};
        assertEquals(true, OXProgram.checkX(table, currentPlayer));
    }
        
       @Test
        public void testCheckXLPlayerO() {
        char currentPlayer = 'O';
        char table[][] = {{'-', '-', 'O'},
                                                             {'-', 'O', '-'},
                                                             {'O', '-', '-'}};
        assertEquals(true, OXProgram.checkX(table, currentPlayer));
    }
             
        //CheckWinPlayerO
        @Test
    public void testCheckPlayerXWin() {

        char table[][] = {{'O', 'O', 'O'},
                                                             {'-', '-', '-'},
                                                             {'-', '-', '-'}};

        char currentPlayer = 'O';
        int col = 1;
        int row = 1;
        assertEquals(true, OXProgram.checkWin(table, currentPlayer, col, row));
    }

}
